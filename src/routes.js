const {
  addBookHandler,
  getFlexibleBooksHandler,
  editBookByIdHandler,
  deleteBookByIdHandler} = require('./handler');

const routes = [
  {
    method: 'POST',
    path: '/books',
    handler: addBookHandler,
  },
  {
    method: 'GET',
    path: '/books/{idParams?}',
    handler: getFlexibleBooksHandler,
  },
  {
    method: 'PUT',
    path: '/books/{idParams}',
    handler: editBookByIdHandler,
  },
  {
    method: 'DELETE',
    path: '/books/{idParams}',
    handler: deleteBookByIdHandler,
  },
];

module.exports = routes;
