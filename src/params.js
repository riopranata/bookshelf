/* eslint-disable require-jsdoc */
const {nanoid} = require('nanoid');

class Bookparams {
  constructor(add) {
    this.id = nanoid(10);
    this.name = add.name;
    this.year = add.year;
    this.author = add.author;
    this.summary = add.summary;
    this.publisher = add.publisher;
    this.pageCount = add.pageCount;
    this.readPage = add.readPage;
    this.finished = add.readPage === add.pageCount;
    this.reading = add.reading;
    const currentTime = new Date().toISOString();
    this.insertedAt = currentTime;
    this.updatedAt = currentTime;
  }

  updateBookParams(update) {
    this.name = update.name;
    this.year = update.year;
    this.author = update.author;
    this.summary = update.summary;
    this.publisher = update.publisher;
    this.pageCount = update.pageCount;
    this.readPage = update.readPage;
    this.reading = update.reading;
    this.updatedAt = new Date().toISOString();
  }

  getPublisher() {
    return {id: this.id, name: this.name, publisher: this.publisher};
  }
}

module.exports = Bookparams;
